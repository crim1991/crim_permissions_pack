<?php

namespace Crim\Permissions\app\Models\User;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $table = 'role_user';
    protected $fillable = ['role_id', 'user_id'];
    public $timestamps = false;

    public static function createUserRole($last_user_id, $role_id)
    {
        RoleUser::create([
            'user_id' => $last_user_id,
            'role_id' => $role_id
        ]);
    }

    public static function updateRole($user_id, $role_id)
    {
        RoleUser::where('user_id', $user_id)->update([
            'role_id' => $role_id
        ]);
    }
}
