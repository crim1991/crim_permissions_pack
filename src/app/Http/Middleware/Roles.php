<?php
namespace Crim\Permissions\app\Http\Middleware;

use App\Models\User\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Roles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        if (!Auth::check())
            return redirect('auth/login');

        $user = new User();

        foreach($roles as $role) {
            if($user->checkRole($role)) {
                return $next($request);
            }
        }

        Session::flash('no_access', 'No access, please call administrator');

        return redirect('auth/login');
    }
}
