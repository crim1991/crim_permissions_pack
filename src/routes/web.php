<?php

/**
 * Permissions page
 */

Route::get('/panel/permissions', function () {
    return view('crim::permissions/index');
});

Route::group(['prefix' => '/panel/permissions'], function() {
    Route::get('/', function () {
        return view('crim::permissions/index');
    });
});
